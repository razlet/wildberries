-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: razlet
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.41-MariaDB-1~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `act_rows`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `act_rows` (
  `act_uid` varchar(32) DEFAULT NULL,
  `good_uid` varchar(32) DEFAULT NULL,
  `refund_reason` text,
  KEY `act_rows_act_uid_index` (`act_uid`),
  CONSTRAINT `act_rows__uid_fk` FOREIGN KEY (`act_uid`) REFERENCES `acts` (`uid`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_rows`
--


--
-- Table structure for table `acts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acts` (
  `uid` char(32) NOT NULL,
  `responsible_completer` char(255) DEFAULT NULL,
  `responsible_sorter` char(255) DEFAULT NULL,
  `type` char(24) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `table_name_uid_uindex` (`uid`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acts`
--


--
-- Table structure for table `boxes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boxes` (
  `uid` varchar(32) NOT NULL,
  `date` datetime DEFAULT NULL,
  `act_uid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `boxes_uid_uindex` (`uid`),
  KEY `boxes_uid_index` (`uid`),
  CONSTRAINT `boxes__uid_fk` FOREIGN KEY (`uid`) REFERENCES `acts` (`uid`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boxes`
--


--
-- Table structure for table `delivery`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery` (
  `uid` varchar(32) NOT NULL,
  `order_wildberries` text,
  `cell` int(11) DEFAULT NULL,
  `receive_date` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `delivery_uid_uindex` (`uid`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery`
--


--
-- Table structure for table `goods`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods` (
  `uid` varchar(32) NOT NULL,
  `delivery_uid` varchar(32) DEFAULT NULL,
  `status` text,
  `refund_reason` text,
  `phone` varchar(11) DEFAULT NULL,
  `passport` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `goods_uid_uindex` (`uid`),
  KEY `goods_delivery_uid_index` (`delivery_uid`),
  CONSTRAINT `goods_delivery_uid_fk` FOREIGN KEY (`delivery_uid`) REFERENCES `delivery` (`uid`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-09 17:59:02
