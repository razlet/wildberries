<?php


namespace Razlet\Wildberries\Entity;

use DateTime;

/**
 * Коробка доставки
 * @package Razlet\Wildberries\Entity
 * @property string $uid идентификатор коробки
 * @property DateTime $date
 */
class Box extends Entity
{

}
