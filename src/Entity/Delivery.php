<?php


namespace Razlet\Wildberries\Entity;

use DateTime;

/**
 * Class Package
 * @package Razlet\Wildberries\Entity
 * @property string $uid
 * @property string $order
 * @property array $goods
 * @property string $cell
 * @property DateTime $receive_date
 */
class Delivery extends Entity
{

}