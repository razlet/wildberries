<?php


namespace Razlet\Wildberries\Entity;

/**
 * Class Good
 * @package Razlet\Wildberries\Entity
 * @property string $uid id в системе wildberries
 * @property Delivery $delivery
 * @property string $status
 * @property string $refundReason
 * @property \DateTime $soldDate
 * @property $phone - номер телефона
 * @property $passport - четы цифры паспорта
 */
class Good extends Entity
{
    const STATUS_IN_CELL = 'IN_CELL';
    const STATUS_GIVE_OUT = 'GIVE_OUT';
    const STATUS_IN_REFUND_BOX = 'IN_REFUND_BOX';

    const UNREFUNDED = '';
    const VOLUNTARY_REFUND = 'VOLUNTARY';
    const GARANTIUS_REFUND = 'GARANTIUS';
    const GARANTIUS_UNCHECKED_REFUND = 'GARANTIUS_UNCHECKED';
    const ERROR_REFUND = 'ERROR';

}