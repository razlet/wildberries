<?php

namespace Razlet\Wildberries\Model;

use PDO;
use PDOStatement;

abstract class Base
{
    /**
     * @var PDO
     */
    static $pdo;
    /**
     * @var array
     */
    static $statements = [];

    /**
     * Base constructor.
     */
    public function __construct()
    {
        self::$pdo = new PDO('mysql:host=db_khatakon;port=3306;dbname=razlet', 'user', 'pass');
        self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @param $statement
     * @return PDOStatement
     */
    public function prepareStatement($statement)
    {
        if (isset(self::$statements[$statement])) {
            return self::$statements[$statement];
        } else {
            $stmt = self::$pdo->prepare($statement);
            self::$statements[$statement] = $stmt;

            return  $stmt;
        }
    }

    /**
     * @param $statement
     * @param $values
     * @return PDOStatement
     */
    public function doStatement($statement, $values)
    {
        $stmt = $this->prepareStatement($statement);
        $stmt->closeCursor();
        $stmt->execute($values);
        if (in_array($statement, $this->getSyncStatementsArray())) {
            $this->insertSyncCommand($values[0], $this->getCommand($stmt->queryString, $values));
        }

        return $stmt;
    }

    /**
     * @param string $stmtString
     * @param array $values
     * @return string
     */
    private function getCommand(string $stmtString, array $values)
    {
        $command = $stmtString;

        foreach ($values as $key => $value) {
            if (is_string($value)) {
                $value = "'" . $value . "'";
            } elseif (is_array($value)) {
                $value = "'" . implode("','", $value) . "'";
            }

            $command = preg_replace('/[?]/', $value, $command, 1);
        }

        return $command . ';';
    }

    private function insertSyncCommand($uid, $command)
    {
        $date = date('Y-m-d H:i:s');
        $syncUid = md5($uid . $date);
        $stm = self::$pdo->prepare('INSERT INTO sync_hash (`uid`, `date`, `commands`) VALUES (?,?,?)');
        $stm->execute([$syncUid, $date, $command]);
    }

    private function getSyncStatementsArray()
    {
        return [
            BoxManager::$add_box,
            DeliveryManager::$add_delivery,
            GoodManager::$add_good,
            GoodManager::$update_status,
        ];
    }
}
