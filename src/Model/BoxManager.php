<?php

namespace Razlet\Wildberries\Model;

use DateTime;

class BoxManager extends Base
{
    static $add_box = "INSERT INTO boxes (uid, pvz_uid, delivery_codes, date) VALUES (?, ?, ?, ?)";

    private static $get_box = "SELECT * FROM boxes WHERE uid = ?";

    /**
     * @param array $data
     */
    public function addBox(array $data)
    {
        $timestamp = new DateTime('now');
        array_push($data, $timestamp->format('Y-m-d H:i:s'));
        $this->doStatement(self::$add_box, $data);
    }

    /**
     * @param string $boxUid
     * @return mixed
     */
    public function getBox(string $boxUid)
    {
        $stmt = $this->doStatement(self::$get_box, [$boxUid]);

        return $stmt->fetch();
    }

    /**
     * @param string $boxUid
     * @return bool
     */
    public function checkBox(string $boxUid)
    {
        return $this->getBox($boxUid) ? true : false;
    }
}
