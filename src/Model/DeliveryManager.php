<?php

namespace Razlet\Wildberries\Model;

use DateTime;
use PDO;

class DeliveryManager extends Base
{
    static $add_delivery = "INSERT INTO delivery (uid, order_wildberries, cell, phone, passport, fio, receive_date)
                                    VALUES (?, ?, ?, ?, ?, ?, ?)";

    private static $get_deliveries_by_order = "SELECT * FROM delivery WHERE order_wildberries = ?";

    private static $det_deliveries_by_phone = "SELECT * FROM delivery WHERE phone = ?";

    /**
     * @param array $deliveries
     */
    public function addDeliveries(array $deliveries)
    {
        $goodManager = new GoodManager();
        $date = (new DateTime('now'))->format('Y-m-d H:i:s');
        foreach ($deliveries as $delivery) {
            [
                'uid' => $uid,
                'order' => $order,
                'cell' => $cell,
                'phone' => $phone,
                'passport' => $passport,
                'fio' => $fio,
                'goods' => $goods,
            ] = $delivery;
            $data = [$uid, $order, $cell, $phone, $passport, $fio, $date];
            $this->doStatement(self::$add_delivery, $data);
            $goodManager->addGoods($goods, $uid);
        }
    }

    /**
     * @param $order
     * @return array
     */
    private function getDeliveriesByOrder($order)
    {
        $stmt = $this->doStatement(self::$get_deliveries_by_order, [$order]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $phone
     * @return array
     */
    private function getDeliveriesByPhone($phone)
    {
        $stmt = $this->doStatement(self::$det_deliveries_by_phone, [$phone]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param array $data
     * @return array
     */
    public function getDeliveries(array $data)
    {
        $result = [];
        ['order' => $order, 'phone' => $phone] = $data;

        if (!empty($order)) {
            $result = $this->getDeliveriesByOrder($order);
        } elseif (!empty($phone)) {
            $result = $this->getDeliveriesByPhone($phone);
        }

        return $result;
    }
}
