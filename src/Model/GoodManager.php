<?php

namespace Razlet\Wildberries\Model;

use PDO;
use Razlet\Wildberries\Entity\Good;

class GoodManager extends Base
{
    static $add_good = "INSERT INTO goods (uid, delivery_uid, status) VALUES (?, ?, ?)";

    private static $get_goods_by_delivery_uid = "SELECT * FROM goods WHERE delivery_uid = ?";

    static $update_status = "UPDATE goods SET status = ? WHERE uid = ?";
    /**
     * @param array $data
     */
    private function addGood(array $data)
    {
        $this->doStatement(self::$add_good, $data);
    }

    /**
     * @param array $goods
     * @param string $deliveryUid
     */
    public function addGoods(array $goods, string $deliveryUid)
    {
        foreach ($goods as $good) {
            $this->addGood([$good, $deliveryUid, Good::STATUS_IN_CELL]);
        }
    }

    /**
     * @param string $deliveryUid
     * @return array
     */
    public function getGoodsByDeliveryUid(string $deliveryUid)
    {
        $stmt = $this->doStatement(self::$get_goods_by_delivery_uid, [$deliveryUid]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param array $data
     */
    public function giveOutGoods(array $data)
    {
        foreach ($data as $uid) {
            $this->doStatement(self::$update_status, [Good::STATUS_GIVE_OUT, $uid]);
        }
    }

    public function refundGoods(array $data)
    {
        foreach ($data as $uid) {
            $this->doStatement(self::$update_status, [Good::STATUS_IN_REFUND_BOX, $uid]);
        }
    }
}
