<?php


namespace Razlet\Wildberries\Sync;


class CheckHeader
{
    /**
     * @var HeadsTable
     */
    private $headsTable;

    /**
     * CheckHeader constructor.
     * @param HeadsTable $headsTable
     */
    public function __construct(HeadsTable $headsTable)
    {
        $this->headsTable = $headsTable;
    }

    public function getAll()
    {
        $this->headsTable->toStart();
        return $this->headsTable->toArray();
    }

    public function isLast($head)
    {
        if (!$this->headsTable->checkExist($head)) {
            return [
                'status' => 'not_found'
            ];
        }
        $diffs = $this->headsTable->findDiffs($head);
        if (!$diffs) {
            return [
                'status' => 'equal'
            ];
        }
        if (count($diffs) == 1) {
            return $this->createResponseForMerge($diffs[0]);
        } else {
            $date = date('Y-m-d H:i:s');
            $uid = md5(rand().$date);
            $commands = array_map(function ($item){
                return $item['commands'];
            }, $diffs);
            $this->headsTable->createMerge($uid, $date, []);
            return $this->createResponseForMerge([
                'uid' => $uid,
                'date' => $date,
                'commands' => $commands,
            ]);
        }
    }

    private function createResponseForMerge($rows)
    {
        return array_merge(['status' => 'find_head'], $rows);
    }
}