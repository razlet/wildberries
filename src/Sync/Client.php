<?php


namespace Razlet\Wildberries\Sync;


class Client
{
    private $host;

    /**
     * Client constructor.
     * @param $host
     */
    public function __construct($host)
    {
        $this->host = $host;
    }


    public function loadAll(): array
    {
        return $this->read([
            'type' => 'all',
        ]);
    }

    public function loadDiff($head): array
    {
        return $this->read([
            'type' => 'check',
            'head' => $head,
        ]);
    }

    public function sendDiff($uid, $date, $mergeList): array
    {
        if ($date instanceof \DateTime) {
            $date = $date->format('Y-m-d H:i:s');
        }
        return $this->read([
            'type' => 'sync',
            'uid' => $uid,
            'date' => $date,
            'commands' => $mergeList,
        ]);
    }

    private function read($array): array
    {
        $client = stream_socket_client($this->host);
        fwrite($client, json_encode($array));
        $result = stream_get_contents($client);
        fclose($client);
        return json_decode($result, true);
    }
}