<?php


namespace Razlet\Wildberries\Sync;


class CompareNodes
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var HeadsTable
     */
    private $headsTable;

    /**
     * CompareNodes constructor.
     * @param Client $client
     * @param HeadsTable $headsTable
     */
    public function __construct(Client $client, HeadsTable $headsTable)
    {
        $this->client = $client;
        $this->headsTable = $headsTable;
    }

    public function compare()
    {
        $mergeList = [];
        $date = new \DateTime();
        $uid = null;
        $this->headsTable->toStart();
        while ($this->headsTable->next()) {
            $head = $this->headsTable->current();
            $diff = $this->client->loadDiff($head['head']);
            if ($diff['status'] === 'not_found') {
                $mergeList = array_merge($head['commands'], $mergeList);
            }
            if ($diff['status'] === 'equal') {
                break;
            }
            if ($diff['status'] === 'find_head') {
                if (!is_array($diff['commands'])) {
                    $diff['commands'] = [$diff['commands']];
                }
                $uid = $this->headsTable->save($diff);
                break;
            }
            if ($diff['status'] === 'error') {
                return;
            }
        }
        if ($this->headsTable->isEmpty()) {
            $diff =  $this->client->loadDiff('all');
            foreach ($diff as $item) {
                $this->headsTable->createMerge($item['uid'], $item['date'], $item['command']);
            }
        }
        if (!empty($mergeList)) {
            if (null === $uid) {
                $uid = md5(rand().$date->getTimestamp());
                $this->headsTable->createMerge($uid, $date);
            }
            $this->client->sendDiff($uid, $date, $mergeList);
        }
    }
}