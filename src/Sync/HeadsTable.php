<?php


namespace Razlet\Wildberries\Sync;


class HeadsTable
{
    /**
     * @var \PDO
     */
    private $pdo;

    private $array = [];

    private $current = false;

    private $index = 0;

    /**
     * HeadsTable constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function toStart()
    {
        $this->array = $this->pdo
            ->query('SELECT * FROM sync_hash ORDER BY date DESC')
            ->fetchAll(\PDO::FETCH_ASSOC);
        $this->index = 0;
    }

    public function toArray()
    {
        return $this->array;
    }

    public function next()
    {
        $this->current = $this->array[$this->index++] ?? false;
        return $this->current();
    }

    public function current()
    {
        if (!$this->current) {
            return $this->current;
        }
        return [
            'head' => $this->current['uid'],
            'commands' => explode(';', $this->current['commands']),
        ];
    }

    public function save($diff)
    {
        $diff['commands'] = $diff['commands'] ?? [];
        foreach ($diff['commands'] as $command) {
            try {
                $this->pdo->prepare($command)->execute([]);
            }catch (\PDOException $e) {
                echo date('Y-m-d H:i:sP') .' '. $e->getMessage().PHP_EOL;
            }
        }
        $this->createMerge($diff['uid'], $diff['date'], implode(';', $diff['commands'] ?? []));

        return $diff['uid'];
    }

    public function isEmpty()
    {
        return empty($this->array);
    }

    public function createMerge($uid, $date, $command = '')
    {
        if (!is_string($date) && $date instanceof \DateTime) {
            $date = $date->format('Y-m-d H:i:s');
        }
        if (is_array($command)) {
            $command = implode(';', $command);
        }
        $stm = $this->pdo->prepare(
            'INSERT INTO sync_hash (`uid`, `date`, `commands`) VALUES (?,?,?)'
        );
        $stm->execute([$uid, $date, $command]);
    }

    public function checkExist($head)
    {
        $stm = $this->pdo->prepare('SELECT `uid` FROM sync_hash WHERE `uid`=?');
        $stm->execute([$head]);
        $r = $stm->fetch(\PDO::FETCH_COLUMN);
        return !empty( $r );
    }

    public function findDiffs($head)
    {
        $stm = $this->pdo->prepare('SELECT `date` FROM sync_hash WHERE `uid`=?');
        $stm->execute([$head]);
        $date = $stm->fetch(\PDO::FETCH_COLUMN);

        if (!empty($date)) {
            $stm = $this->pdo->prepare('SELECT * FROM sync_hash WHERE `date`>=? AND `uid`<>?');
            $stm->execute([$date, $head]);
            $r =  $stm->fetchAll(\PDO::FETCH_ASSOC);
            return $r;
        }

        return false;
    }
}