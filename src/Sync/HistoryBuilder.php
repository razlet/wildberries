<?php


namespace Razlet\Wildberries\Sync;


class HistoryBuilder
{

    /**
     * @var \PDO
     */
    private $pdo;

    private $withExecCommands = false;

    public function save($commands)
    {
        if ($this->withExecCommands) {
            foreach ($commands as $command) {
                $this->pdo->query($command);
            }
        }
        $dateTime = new \DateTime();
        $uid = uniqid($dateTime->format('Y-m-d H:i:s').json_encode($commands));
        $this->pdo->prepare(
            'INSERT INTO sync VALUES (`uid`, `date`, `commands`) VALUES (?,?,?)'
        )->execute([$uid, $dateTime->format('Y-m-d H:i:s'), implode(';', $commands)]);

        return $uid;
    }

    
}