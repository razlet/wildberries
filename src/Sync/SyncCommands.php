<?php


namespace Razlet\Wildberries\Sync;


class SyncCommands
{
    /**
     * @var CheckHeader
     */
    private $checkHeader;

    /**
     * SyncCommands constructor.
     * @param CheckHeader $checkHeader
     */
    public function __construct(CheckHeader $checkHeader)
    {
        $this->checkHeader = $checkHeader;
    }


    public function sync($array)
    {
        $last = $this->checkHeader->isLast($array['start_head']);
        if ($last['status'] && empty($last['commands'])) {
            foreach ($last['commands'] as $command) {
                $this->doCommand($command);
            }
            $this->createPoint($last['commands']);
        }
    }
}