<?php
include_once __DIR__.'/../../vendor/autoload.php';

$socket = stream_socket_server("tcp://0.0.0.0:8100", $errno, $errstr);
if (!$socket) {
    echo "$errstr ($errno)<br />\n";
} else {
    $headsTable = new \Razlet\Wildberries\Sync\HeadsTable(include __DIR__ . '/../../config/pdo.php');
    $checker = new \Razlet\Wildberries\Sync\CheckHeader($headsTable);
    while ($conn = stream_socket_accept($socket, 3600)) {
        $syncRequest = @json_decode(fread($conn, 1024), true);
        switch ($syncRequest['type']) {
            case 'all':
                $result = $checker->getAll();
                break;
            case 'check':
                $result = $checker->isLast($syncRequest['head']);
                break;
            case 'sync':
                $result = [
                    'status' => 'sync',
                    $headsTable->save($syncRequest)
                ];
                break;
        }
        fwrite($conn, json_encode($result));
        fclose($conn);
    }
    fclose($socket);
}