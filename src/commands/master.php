<?php

include __DIR__ . '/../../vendor/autoload.php';

$headsTable = new \Razlet\Wildberries\Sync\HeadsTable(
    include __DIR__.'/../../config/pdo.php'
);
$clients = [
    new \Razlet\Wildberries\Sync\CompareNodes(
        new \Razlet\Wildberries\Sync\Client('tcp://192.168.0.137:8102'),
        $headsTable
    ),
];

/** @var \Razlet\Wildberries\Sync\CompareNodes $client */
//foreach ($clients as $client) {
$clients[0]->compare();
//}
