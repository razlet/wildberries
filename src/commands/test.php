<?php
include __DIR__ . '/../../vendor/autoload.php';

$headsTable = new \Razlet\Wildberries\Sync\HeadsTable(include __DIR__ . '/../../config/pdo.php');
//$headsTable->createMerge(uniqid(), new \DateTime());
//$headsTable->createMerge(
//    uniqid(),
//    new \DateTime(),
//    ['INSERT INTO boxes (`uid`, `date`, `act_uid`) VALUES ("'.uniqid().'", "2020-05-20", "111")']
//);

$headsTable->save([
    'uid' => uniqid(),
    'date' => date('Y-m-d H:i:s'),
    'commands' => [
        'INSERT INTO boxes (`uid`, `date`, `act_uid`) VALUES ("'.uniqid().'", "2020-05-20 00:00:00", null)',
        'INSERT INTO boxes (`uid`, `date`, `act_uid`) VALUES ("'.uniqid().'", "2020-05-21 00:00:00", null)'
    ]
]);

$headsTable->toStart();
var_dump($headsTable->toArray());