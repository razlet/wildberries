<?php

use Razlet\Wildberries\Model\BoxManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

const UID_PVZ = 'ludmta';

$request = Request::createFromGlobals();
$boxManager = new BoxManager();
[
    'uid' => $boxUid,
    'uid_pvz' => $uidPvz,
    'delivery_codes' => $deliveryCodes,
] = json_decode($request->getContent(), true);


if (UID_PVZ !== $uidPvz){
    $result = ['status' => 'error', 'message' => 'Коробка для другого ПВЗ'];
} elseif (!$boxManager->checkBox($boxUid)) {
    $boxManager->addBox([$boxUid, $uidPvz, implode(',', $deliveryCodes)]);
    $result = ['status' => 'OK'];
} else {
    $result = ['status' => 'error', 'message' => 'Коробка уже зарегистрирована'];
}

$response = new JsonResponse($result);
$response->send();
