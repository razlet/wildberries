<?php

use Razlet\Wildberries\Model\DeliveryManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals();
$deliveryManager = new DeliveryManager();
$data = json_decode($request->getContent(), true);
['deliveries' => $deliveries] = $data;

try {
    $deliveryManager->addDeliveries($deliveries);
    $result = ['status' => 'OK'];
} catch (PDOException $e) {
    $result = ['status' => 'error', 'message' => $e->getMessage()];
}

$response = new JsonResponse($result);
$response->send();