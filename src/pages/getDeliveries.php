<?php

use Razlet\Wildberries\Model\DeliveryManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals();
$data = json_decode($request->getContent(), true);
$manager = new DeliveryManager();
$deliveries = $manager->getDeliveries($data);

$response = new JsonResponse($deliveries);
$response->send();
