<?php

use Razlet\Wildberries\Model\GoodManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

$result = [];
$request = Request::createFromGlobals();
$data = json_decode($request->getContent(), true);
['deliveries' => $deliveries] = $data;

$manager = new GoodManager();
foreach ($deliveries as $uid) {
    $result[$uid] = $manager->getGoodsByDeliveryUid($uid);;
}

$response = new JsonResponse($result);
$response->send();