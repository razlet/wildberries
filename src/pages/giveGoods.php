<?php

use Razlet\Wildberries\Model\GoodManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals();
$data = json_decode($request->getContent(), true);
['goods' => $goods] = $data;
$manager = new GoodManager();

try {
    $manager->giveOutGoods($goods);
    $result = ['status' => 'OK'];
} catch (PDOException $e) {
    $result = ['status' => 'error', 'message' => $e->getMessage()];
}

$response = new JsonResponse($result);
$response->send();